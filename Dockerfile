FROM node:14-slim as build
RUN apt update && \
    apt install -y \
    git

WORKDIR /app

COPY . /app
RUN npm install
RUN node_modules/.bin/grunt
RUN ls -la /app
FROM nginx:1-alpine
COPY --from=build /app/build /usr/share/nginx/html
